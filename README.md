Download, set up config.yml for Themekit, and you're off.

--

To update Bulma, download the updated version and run the below command to convert SASS to SCSS, and delete the CSS files. Run this from the new Bulma directory before copying over.

`sass-convert -R DIRECTORY --from sass --to scss && find . -type f -name '*.sass' -delete && find . -type f -name '*.scss' -exec sed -i '' s/sass/scss/ {} +`

This will also automatically update all imports from sass to scss.
